==================================================================
https://keybase.io/ammonkey
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://ammonsmith.me
  * I am ammonkey (https://keybase.io/ammonkey) on keybase.
  * I have a public key with fingerprint 060D 9A2C 653E 9A31 6571  6C7D DE0E 2BD0 2C3C F0C7

To do so, I am signing this object:

{
    "body": {
        "key": {
            "eldest_kid": "01016d05c0e5bb37e7c6d4e0790314e26b8345ae6ecd9b792d79cf4c22480be929c60a",
            "fingerprint": "060d9a2c653e9a3165716c7dde0e2bd02c3cf0c7",
            "host": "keybase.io",
            "key_id": "de0e2bd02c3cf0c7",
            "kid": "01016d05c0e5bb37e7c6d4e0790314e26b8345ae6ecd9b792d79cf4c22480be929c60a",
            "uid": "66791d9f28c9bb3261d5683f15aa7d19",
            "username": "ammonkey"
        },
        "service": {
            "hostname": "ammonsmith.me",
            "protocol": "https:"
        },
        "type": "web_service_binding",
        "version": 1
    },
    "ctime": 1496597449,
    "expire_in": 157680000,
    "prev": "0f0c93d6a3c0150afae01cef1d6fd483b0549a637b30cffb2ad3f62fb1df1206",
    "seqno": 3,
    "tag": "signature"
}

which yields the signature:

-----BEGIN PGP MESSAGE-----
Version: Keybase OpenPGP v2.0.71
Comment: https://keybase.io/crypto

yMM/AnicrVJ/TFVlGL6UXAIFMTUogeAkCHK59/z8zj1XxfAmDMXGkK1hye38+A4c
5J5zvT9QoFtiIVzvQKpNySZSshlsRDRa4a5TkBXkGtdKhwWotw2HFcEEnJGrc1ht
/dGfff98e9/veZ7veZ+9Q9GP66LCJmMyg4ats1+FXR1o9ehKSOtILcIpQjViqUUO
wOULVgrQ5bYdkATEgqAYigEBpXgUUhxH0JDmgUBClGZQAiMhDjgzQVIsBJAXGI5m
cIFmeJHkcZw0oxxkcIYHKIsYEFGSy6DT4ZRktyYLUIFhcR5QBGRYAgMUjQGeFgSI
QpwTUJwneBHlaZVYrrg0hmqOY13QKClqTy1sy/b+A/8/+/YsywFAM5jAiLiZZ1Q5
HGACBcyEiFEsSwsYowFd0CmzdqiiWbtdkbU0vQZE7VZJPNSC1Sb5N8Jll9zlRrU2
IA6n4lZ4pVJ9Kne7HS6LRnVXOzTsIcjZ/laxcZIsqEGqjCrodEmKjFgwFcm7JU0W
IxlAMTRJMgYEHnZITmiTNARFAzOqHu0fWKWlo2bFEAJgCR7FKJQVWYhiPBQxAYgC
aSY4lCIZFhA0R6C8KHI4KxAiwEUOE0QMRwGizXVQVhALodpky1RJl1Qms26PEyLe
wYFXVujConT68Me0BdNFRcb+s3aXM1f/Wfn2Or3hbueSNW8e2Ws3J67yC/wXzX0V
xclDcaC9d8PGW2l3n39Of840OmCO9Bjb0ksfthT+kHb77K6RD7YHstOn5sdmqZmC
Ucd4blxNThLXx7cX+yuXsMJf9sauivns6PT1/JR3yNn6O3W1t20//WyNWB/UJ1si
mps+9sU+yZ1J6Dp1uaKj8w2FqmtYmjTJirjoCZ6oXRHaPNfjyu4ijq1m/GXxxubg
7pWNvY8+Oq9nexZT3o/vvDiz58P9DzD32c6MQ8P8jjwxx3qyqeDbnZWhknttMVPv
tXaZ1j79alH1vavbJEa8/m7Bj/ef8q0LfBJtO3L6rX2m5Kis0nwmVJjltc70eBdq
Ci58uSFvy81Pe8OHxgz98y/4shLnIr2WfeH5O8+HXNsFkzMwEr10qb8l0GHNaPht
+s7K2eG1RT0PTUH4YqExY1duPqhZfDC2mHv8taRNoWtKkqdtak2b74n4jaemz4wP
HJn7o3Fris944fDu1J6XifLsi2UdVyK7O+6nlq7fMdFVscWAZJTXZb+02SN7k68k
fVdKPVtUVU91p0a8fmJBf2O0V7cN27MUd26TeHMyoS/gD7252H1N/PVzqmWmOUF+
lN0+kQUyF/ye8b6Tt7Jy+n8vnvj6kv379MGjTS3+Bkv9wdYiwzNromHj4HADu9/3
TUnaacPxgNQXfiNx9C+zx7iz
=MME1
-----END PGP MESSAGE-----

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/ammonkey

==================================================================

